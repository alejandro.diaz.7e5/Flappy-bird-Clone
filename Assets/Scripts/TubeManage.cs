using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeManage : MonoBehaviour
{
    public GameObject[] tubes;
    public int totalTubes;
    public int tubeSpeed = 10;
    public int nextTube = 0;
    public float timeInterval = 5;
    public float timeCurrent;
    // Start is called before the first frame update
    void Start()
    {
        totalTubes = tubes.Length;
    }

    // Update is called once per frame
    void Update()
    {
        timeCurrent += Time.deltaTime;
        if (timeCurrent > timeInterval)
        {
            tubes[nextTube].transform.position = new Vector3(20, Random.Range(0,-6f));
            timeCurrent = 0;
            nextTube = (nextTube == totalTubes - 1) ? 0 : nextTube + 1;
        }
        foreach (GameObject tube in tubes)
        {
            tube.transform.position -= new Vector3(tubeSpeed * Time.deltaTime, 0);
        }


    }
}
