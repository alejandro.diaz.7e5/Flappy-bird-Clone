using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillerTube : MonoBehaviour
{
    public UIManagerGame UIswitch;
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.GetComponent<Rigidbody2D>().AddForce(new Vector2(-10, -100), ForceMode2D.Impulse);
            UIswitch.GameOver();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        UIswitch = GameObject.Find("Canvas").GetComponent<UIManagerGame>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
