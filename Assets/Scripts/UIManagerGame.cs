using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManagerGame : MonoBehaviour
{
    GameObject gameOver;
    GameObject score;
    // Start is called before the first frame update
    void Start()
    {
        score = GameObject.Find("Score");
        gameOver = GameObject.Find("GameOver");
        gameOver.SetActive(false);
    }
    public void GameOver()
    {
        gameOver.SetActive(true);
        GameObject.Find("GameOverScore").GetComponent<TMP_Text>().text = score.GetComponent<TMP_Text>().text;
    }
    public void ReloadLevel()
    {
        Scene reload = SceneManager.GetActiveScene();
        SceneManager.LoadScene(reload.buildIndex);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
