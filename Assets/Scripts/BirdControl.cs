using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdControl : MonoBehaviour
{

    public float VInput;
    float jumpHeight = 60;
    float jumpCD = 0;
    Rigidbody2D rB;
    // Start is called before the first frame update
    void Start()
    {
        rB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        VInput = Input.GetAxis("Vertical");
        if (VInput > 0 && jumpCD <= 0)
        {
            jumpCD = 0.4f;
             //rB.AddForce(new Vector2(0, jumpHeight),ForceMode2D.Impulse);
             rB.velocity = new Vector2(0, 15);
        }

        if (jumpCD >= 0) 
        jumpCD -= Time.deltaTime;
    }
}
