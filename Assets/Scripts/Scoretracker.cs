using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scoretracker : MonoBehaviour
{
    public int score = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<TMP_Text>().text = "Score: " + score;
    }
}
